package com.example.chatmessage;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    ArrayList<Chat> listChats;
    RecyclerView rvChats;
    Adapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        listChats = new ArrayList<>();

        Chat chat1 = new Chat();
        chat1.name = "James";
        chat1.message = "Thank you! That was very helpful!";
        chat1.thumbnail = R.drawable.pp1;

        Chat chat2 = new Chat();
        chat2.name = "Will Kenny";
        chat2.message = "I know... I'm trying to get the funds";
        chat2.thumbnail = R.drawable.pp2;

        Chat chat3 = new Chat();
        chat3.name = "Beth Williams";
        chat3.message = "I'm looking for tips around capturing the milky way. I have a 60 with a 24-100mm...";
        chat3.thumbnail = R.drawable.pp3;

        Chat chat4 = new Chat();
        chat4.name = "Rev Shawn";
        chat4.message = "Wanted to ask if you're available for a portrait shot next week";
        chat4.thumbnail = R.drawable.pp4;

        listChats.add(chat1);
        listChats.add(chat2);
        listChats.add(chat3);
        listChats.add(chat4);

        adapter = new Adapter(this, listChats);

        rvChats = findViewById(R.id.rvChats);

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        rvChats.setLayoutManager(linearLayoutManager);
        rvChats.setAdapter(adapter);
    }
}
